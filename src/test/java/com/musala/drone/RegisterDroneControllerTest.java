package com.musala.drone;

import com.musala.drone.dto.request.DroneRegistrationRequest;
import com.musala.drone.enums.Model;
import com.musala.drone.enums.State;
import com.musala.drone.model.Drone;
import com.musala.drone.repository.DroneRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RegisterDroneControllerTest extends BaseIntegrationTest{

    @Autowired
    private DroneRepository droneRepository;

    @Test
    public void registerDroneTest_success() throws Exception {

        DroneRegistrationRequest registrationRequest = DroneRegistrationRequest.builder()
                .model(Model.LightWeight)
                .serialNumber(RandomStringUtils.randomAlphabetic(10))
                .weightLimit(500.00)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.
                        post("http://localhost:9090/api/v1/drone/register")
                        .content(asJsonString(registrationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void registerDroneTest_failOnSerialNumberAlphaNumeric() throws Exception {

        DroneRegistrationRequest registrationRequest = DroneRegistrationRequest.builder()
                .model(Model.LightWeight)
                .serialNumber(RandomStringUtils.randomAlphanumeric(5) + RandomStringUtils.randomNumeric(5))
                .weightLimit(500.00)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.
                        post("http://localhost:9090/api/v1/drone/register")
                        .content(asJsonString(registrationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void registerDroneTest_failOnSerialNumberGreaterThan100() throws Exception {

        DroneRegistrationRequest registrationRequest = DroneRegistrationRequest.builder()
                .model(Model.LightWeight)
                .serialNumber(RandomStringUtils.randomAlphabetic(101))
                .weightLimit(500.00)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.
                        post("http://localhost:9090/api/v1/drone/register")
                        .content(asJsonString(registrationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void registerDroneTest_failOnWeightLimitGreaterThan500() throws Exception {

        DroneRegistrationRequest registrationRequest = DroneRegistrationRequest.builder()
                .model(Model.LightWeight)
                .serialNumber(RandomStringUtils.randomAlphabetic(101))
                .weightLimit(600.00)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.
                        post("http://localhost:9090/api/v1/drone/register")
                        .content(asJsonString(registrationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void registerDroneTest_failOnDuplicateSerialNumber() throws Exception {

        registerDrone();

        DroneRegistrationRequest registrationRequest = DroneRegistrationRequest.builder()
                .model(Model.LightWeight)
                .serialNumber("qwertyuiop")
                .weightLimit(500.00)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.
                        post("http://localhost:9090/api/v1/drone/register")
                        .content(asJsonString(registrationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andDo(print())
                .andReturn();
    }

    public void registerDrone() {
        Drone drone = Drone.builder()
                .serialNumber("qwertyuiop")
                .model(Model.LightWeight)
                .batteryCapacity(BigDecimal.valueOf(100L))
                .state(State.IDLE)
                .weightLimit(500.00)
                .build();

        droneRepository.save(drone);
    }
}
