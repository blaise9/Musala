package com.musala.drone.enums;

public enum Model {
    LightWeight, MiddleWeight, CruiserWeight, HeavyWeight
}
