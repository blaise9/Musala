package com.musala.drone.util;

import com.musala.drone.common.ErrorMessage;
import com.musala.drone.enums.State;
import com.musala.drone.exception.BatteryLevelException;
import com.musala.drone.exception.WrongStateException;
import com.musala.drone.exception.OverWeightException;

import java.math.BigDecimal;

public class ValidatorUtil {
    public static void validateDroneIsNotOverWeightLimit(double weightLimit, double currentWeight, double newWeight){
        if(!(weightLimit >= (currentWeight + newWeight))){
            throw new OverWeightException(String.format(ErrorMessage.WEIGHT_LIMIT_EXCEEDED, weightLimit));
        }
    }

    public static void validateBatteryLevel(BigDecimal batteryLevel){
        if(!(BigDecimal.valueOf(25L).compareTo(batteryLevel) <= 0)){
            throw new BatteryLevelException(ErrorMessage.BATTERY_LEVEL_LOW);
        }
    }

    public static void validateDroneIsInIdleOrLoadingState(State state) {
        if(!(State.IDLE.equals(state) || State.LOADING.equals(state))){
            throw new WrongStateException(ErrorMessage.WRONG_STATE_EXCEPTION);
        }
    }
}
