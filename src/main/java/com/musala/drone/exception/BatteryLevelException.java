package com.musala.drone.exception;

public class BatteryLevelException extends RuntimeException {

    public BatteryLevelException(String message) {
        super(message);
    }
}
