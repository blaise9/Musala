package com.musala.drone.exception;

public class WrongStateException extends RuntimeException {

    public WrongStateException(String message) {
        super(message);
    }
}
