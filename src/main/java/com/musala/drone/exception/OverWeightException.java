package com.musala.drone.exception;

public class OverWeightException extends RuntimeException {

    public OverWeightException(String message) {
        super(message);
    }
}
