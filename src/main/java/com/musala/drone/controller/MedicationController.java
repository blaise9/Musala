package com.musala.drone.controller;

import com.musala.drone.common.constant.Mappings;
import com.musala.drone.dto.response.DroneMedicationItemsResponse;
import com.musala.drone.service.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Mappings.API_V1 + Mappings.MEDICATION_BASE)
@RequiredArgsConstructor
public class MedicationController {

    private final MedicationService medicationService;

    @GetMapping
    public DroneMedicationItemsResponse getAllDroneMedicationItems(@RequestParam String serialNumber){
        return medicationService.getAllDroneMedicationItems(serialNumber);
    }

}
