package com.musala.drone.controller;

import com.musala.drone.common.constant.Mappings;
import com.musala.drone.dto.request.DroneRegistrationRequest;
import com.musala.drone.dto.request.LoadDroneRequest;
import com.musala.drone.dto.response.DroneBatteryLevelResponse;
import com.musala.drone.dto.response.DroneResponse;
import com.musala.drone.dto.response.LoadDroneResponse;
import com.musala.drone.exception.BadRequestException;
import com.musala.drone.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = Mappings.API_V1 + Mappings.DRONE_BASE)
@RequiredArgsConstructor
public class DroneController {

    private final DroneService droneService;

    @PostMapping(value = Mappings.REGISTER_DRONE)
    public DroneResponse registerDrone(@Validated @RequestBody DroneRegistrationRequest registrationRequest) throws BadRequestException {
        return droneService.registerDrone(registrationRequest);
    }

    @PostMapping(value = Mappings.LOAD_DRONE)
    public LoadDroneResponse loadDroneWithMedicationItem(@Validated @RequestBody LoadDroneRequest loadDroneRequest){
        return droneService.loadDroneWithMedicationItem(loadDroneRequest);
    }

    @GetMapping(value = Mappings.GET_ALL_AVAILABLE_DRONES)
    public List<DroneResponse> getAllAvailableDrones(){
        return droneService.getAllAvailableDronesForLoading();
    }

    @GetMapping(value = Mappings.GET_DRONE_BATTERY_LEVEL)
    public DroneBatteryLevelResponse getDroneBatteryLevel(@RequestParam String serialNumber){
        return droneService.getDroneBatteryLevel(serialNumber);
    }
}
