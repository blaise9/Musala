package com.musala.drone.job;

import com.musala.drone.model.Drone;
import com.musala.drone.service.DroneBatteryLevelAuditLogService;
import com.musala.drone.service.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class BatteryLevelLogScheduler {

    private final DroneService droneService;
    private final DroneBatteryLevelAuditLogService droneBatteryLevelAuditLogService;

    @Value("${battery.level.job.schedule.record.limit:100}")
    private int recordLimit;

    @Scheduled(cron = "${cron.process.job.schedule:0 * * * * ?}") //Every hour
    public void runBatteryLevelCheck() {
        log.info("Running battery level check job");

        Pageable pageRequest = PageRequest.of(0, recordLimit);

        boolean isLastPage = false;
        while (!isLastPage) {

            Page<Drone> dronesEntries = droneService.findAll(pageRequest);

            logOrUpdateBatteryLevel(dronesEntries.getContent());

            isLastPage = dronesEntries.isLast();

            if(!isLastPage){
                pageRequest = pageRequest.next();
            }
        }

        log.info("Successfully ran battery level check job");
    }

    private void logOrUpdateBatteryLevel(List<Drone> content) {
        for(Drone drone : content){
            droneBatteryLevelAuditLogService.logBatteryLevel(drone);
        }
    }
}
