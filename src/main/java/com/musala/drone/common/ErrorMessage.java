package com.musala.drone.common;

public class ErrorMessage {

    public static String SERIAL_NUMBER_NOT_FOUND = "Drone with serial number [%s] not found";
    public static String WEIGHT_LIMIT_EXCEEDED = "Drone weight limit exceeded, weight limit is %s";
    public static String BATTERY_LEVEL_LOW = "Battery level is currently lower than 25%";
    public static String WRONG_STATE_EXCEPTION = "Drone not in idle or loading state";

    public static String DUPLICATE_SERIAL_NUMBER = "Drone with serial number [%s] already exist";

}
