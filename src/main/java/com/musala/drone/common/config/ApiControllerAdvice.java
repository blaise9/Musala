package com.musala.drone.common.config;

import com.musala.drone.common.DroneServiceResponse;
import com.musala.drone.exception.*;
import com.musala.drone.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ApiControllerAdvice extends ResponseEntityExceptionHandler {

    private final ResponseUtil responseUtil;

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Method Argument Not Valid Exception : ", ex);
        String errorMessage = ex.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst()
                .orElse(ex.getMessage());
        return handleExceptionInternal(ex, errorMessage,
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);

    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public DroneServiceResponse handleNotFoundException(NotFoundException ex) {
        log.error("Resource Not Found Exception : ", ex);
        return responseUtil.buildErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler({BatteryLevelException.class, OverWeightException.class, WrongStateException.class})
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public DroneServiceResponse handleBatteryLevelException(Exception exception) {
        log.error("An error occurred : ", exception);
        return responseUtil.buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, exception.getMessage());
    }

    @ExceptionHandler({BadRequestException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public DroneServiceResponse handleBadRequestException(BadRequestException exception) {
        log.error("Bad request exception : ", exception);
        return responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public DroneServiceResponse handleException(Exception exception) {
        log.error("Internal server error : ", exception);
        return responseUtil.buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage());
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        super.handleExceptionInternal(ex,body,headers,status, request);
        log.error(ex.toString());
        String message = (body != null)?body.toString(): ex.getMessage();
        DroneServiceResponse response = responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, message);
        return new ResponseEntity<>(response, status);

    }
}
