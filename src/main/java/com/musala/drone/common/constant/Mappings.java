package com.musala.drone.common.constant;

public class Mappings {
    public static final String API_V1 = "/api/v1";
    public static final String DRONE_BASE = "/drone";
    public static final String MEDICATION_BASE = "/medications";

    //DRONE
    public static final String REGISTER_DRONE = "/register";
    public static final String LOAD_DRONE = "/load";
    public static final String GET_ALL_AVAILABLE_DRONES = "/available/all";
    public static final String GET_DRONE_BATTERY_LEVEL = "/get-drone-battery-level";
}
