package com.musala.drone.service;

import com.musala.drone.dto.request.LoadDroneRequest;
import com.musala.drone.dto.response.DroneMedicationItemsResponse;
import com.musala.drone.model.Drone;
import com.musala.drone.model.Medication;

public interface MedicationService {
    Medication loadDrone(Drone drone, LoadDroneRequest loadDroneRequest);

    DroneMedicationItemsResponse getAllDroneMedicationItems(String serialNumber);

}
