package com.musala.drone.service.impl;


import com.musala.drone.model.Drone;
import com.musala.drone.model.DroneBatteryLevelAuditLog;
import com.musala.drone.repository.DroneBatteryLevelAuditLogRepository;
import com.musala.drone.service.DroneBatteryLevelAuditLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class DroneBatteryLevelAuditLogServiceImpl implements DroneBatteryLevelAuditLogService {

    private final DroneBatteryLevelAuditLogRepository repository;

    @Override
    public DroneBatteryLevelAuditLog logBatteryLevel(Drone drone) {
        return repository.save(DroneBatteryLevelAuditLog.builder()
                        .batteryCapacity(drone.getBatteryCapacity())
                        .serialNumber(drone.getSerialNumber())
                        .runDate(LocalDateTime.now())
                        .build());
    }
}
