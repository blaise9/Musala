package com.musala.drone.service.impl;

import com.musala.drone.common.ErrorMessage;
import com.musala.drone.dto.request.DroneRegistrationRequest;
import com.musala.drone.dto.request.LoadDroneRequest;
import com.musala.drone.dto.response.DroneBatteryLevelResponse;
import com.musala.drone.dto.response.DroneResponse;
import com.musala.drone.dto.response.LoadDroneResponse;
import com.musala.drone.enums.State;
import com.musala.drone.exception.BadRequestException;
import com.musala.drone.exception.NotFoundException;
import com.musala.drone.model.Drone;
import com.musala.drone.model.Medication;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.service.DroneService;
import com.musala.drone.service.MedicationService;
import com.musala.drone.util.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;

    private final MedicationService medicationService;

    @Override
    public DroneResponse registerDrone(DroneRegistrationRequest registrationRequest) throws BadRequestException {

        validateDroneDoesNotExist(registrationRequest.getSerialNumber());

        Drone drone = buildDrone(registrationRequest);

        droneRepository.save(drone);
        return buildDroneResponse(drone);
    }

    private void validateDroneDoesNotExist(String serialNumber) throws BadRequestException {
        Optional<Drone> optional = droneRepository.findBySerialNumber(serialNumber);
        if(optional.isPresent()){
            throw new BadRequestException(ErrorMessage.DUPLICATE_SERIAL_NUMBER);
        }
    }

    @Transactional
    @Override
    public LoadDroneResponse loadDroneWithMedicationItem(LoadDroneRequest loadDroneRequest) {
        Drone drone = droneRepository.findBySerialNumber(loadDroneRequest.getDroneSerialNumber()).orElseThrow
                (() -> new NotFoundException(String.format(ErrorMessage.SERIAL_NUMBER_NOT_FOUND, loadDroneRequest.getDroneSerialNumber())));

        validateDroneCanBeLoaded(loadDroneRequest.getWeight(), drone);

        Medication medicationItem = medicationService.loadDrone(drone, loadDroneRequest);

        updateDroneDetails(drone, loadDroneRequest.getWeight());

        return buildResponse(medicationItem);
    }

    @Override
    public List<DroneResponse> getAllAvailableDronesForLoading() {
        List<Drone> drones = droneRepository.findAllByStateIn(Arrays.asList(State.IDLE, State.LOADING));

        Function<Drone, DroneResponse> getAvailableDrones = (drone) -> DroneResponse.builder()
                .serialNumber(drone.getSerialNumber())
                .model(drone.getModel())
                .batteryCapacity(drone.getBatteryCapacity())
                .weightLimit(drone.getWeightLimit())
                .currentWeight(formatCurrentWeight(drone.getCurrentWeight()))
                .state(drone.getState())
                .build();

        return drones.stream().map(getAvailableDrones).collect(Collectors.toList());
    }

    @Override
    public DroneBatteryLevelResponse getDroneBatteryLevel(String serialNumber) {
        Drone drone = droneRepository.findBySerialNumber(serialNumber).orElseThrow(() ->
                new NotFoundException(String.format(ErrorMessage.SERIAL_NUMBER_NOT_FOUND, serialNumber)));
        return DroneBatteryLevelResponse.builder().batteryLevel(drone.getBatteryCapacity()).build();
    }

    @Override
    public Page<Drone> findAll(Pageable pageRequest) {
        return droneRepository.findAll(pageRequest);
    }

    private Drone buildDrone(DroneRegistrationRequest registrationRequest) {
        return Drone.builder()
                .serialNumber(registrationRequest.getSerialNumber())
                .model(registrationRequest.getModel())
                .weightLimit(registrationRequest.getWeightLimit())
                .currentWeight(0)
                .state(State.IDLE)
                .batteryCapacity(new BigDecimal(100))
                .build();
    }

    private DroneResponse buildDroneResponse(Drone drone){
        return DroneResponse.builder()
                .serialNumber(drone.getSerialNumber())
                .model(drone.getModel())
                .weightLimit(drone.getWeightLimit())
                .currentWeight(formatCurrentWeight(drone.getCurrentWeight()))
                .batteryCapacity(drone.getBatteryCapacity())
                .state(drone.getState())
                .build();
    }

    private void validateDroneCanBeLoaded(double weight, Drone drone) {
        ValidatorUtil.validateDroneIsInIdleOrLoadingState(drone.getState());
        ValidatorUtil.validateBatteryLevel(drone.getBatteryCapacity());
        ValidatorUtil.validateDroneIsNotOverWeightLimit(drone.getWeightLimit(), drone.getCurrentWeight(), weight);
    }

    private void updateDroneDetails(Drone drone, double weight) {
        drone.setCurrentWeight(drone.getCurrentWeight() + weight);
        updateDroneState(drone);
    }

    private Drone updateDroneState(Drone drone) {
        if (State.IDLE.equals(drone.getState())){
            drone.setState(State.LOADING);
        } else if (State.LOADING.equals(drone.getState())){
            if(drone.getWeightLimit() == drone.getCurrentWeight()){
                drone.setState(State.LOADED);
            }
        }
        return droneRepository.save(drone);
    }

    private LoadDroneResponse buildResponse(Medication medicationItem) {
        return LoadDroneResponse.builder()
                .name(medicationItem.getName())
                .code(medicationItem.getCode())
                .image(medicationItem.getImage())
                .weight(medicationItem.getWeight())
                .drone(DroneResponse.builder()
                        .serialNumber(medicationItem.getDrone().getSerialNumber())
                        .model(medicationItem.getDrone().getModel())
                        .batteryCapacity(medicationItem.getDrone().getBatteryCapacity())
                        .state(medicationItem.getDrone().getState())
                        .currentWeight(medicationItem.getDrone().getCurrentWeight())
                        .weightLimit(medicationItem.getDrone().getWeightLimit())
                        .build())
                .build();
    }

    private double formatCurrentWeight(double currentWeight){
        return Math.round(currentWeight * 100.0) / 100.0;
    }
}
