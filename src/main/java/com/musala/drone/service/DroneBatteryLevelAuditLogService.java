package com.musala.drone.service;


import com.musala.drone.model.Drone;
import com.musala.drone.model.DroneBatteryLevelAuditLog;

import java.math.BigDecimal;
import java.util.Optional;

public interface DroneBatteryLevelAuditLogService {
    DroneBatteryLevelAuditLog logBatteryLevel(Drone drone);
}
