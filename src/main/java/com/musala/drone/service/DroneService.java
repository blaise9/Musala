package com.musala.drone.service;

import com.musala.drone.dto.request.DroneRegistrationRequest;
import com.musala.drone.dto.request.LoadDroneRequest;
import com.musala.drone.dto.response.DroneBatteryLevelResponse;
import com.musala.drone.dto.response.DroneResponse;
import com.musala.drone.dto.response.LoadDroneResponse;
import com.musala.drone.exception.BadRequestException;
import com.musala.drone.model.Drone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DroneService {

    DroneResponse registerDrone(DroneRegistrationRequest registrationRequest) throws BadRequestException;

    LoadDroneResponse loadDroneWithMedicationItem(LoadDroneRequest loadDroneRequest);

    List<DroneResponse> getAllAvailableDronesForLoading();

    DroneBatteryLevelResponse getDroneBatteryLevel(String serialNumber);

    Page<Drone> findAll(Pageable pageRequest);
}
